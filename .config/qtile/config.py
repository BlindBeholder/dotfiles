

import os
import re
import socket
import subprocess
from libqtile import qtile, layout, bar, widget, hook, extension
from libqtile.config import Click, Drag, Group, KeyChord, Key, Match, Screen
from libqtile.command import lazy
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
from typing import List

#mod4 or mod = super key
mod = "mod4"
mod1 = "alt"
mod2 = "control"
home = os.path.expanduser('~')
myTerm = 'alacritty'
myBrowser = 'qutebrowser'


keys = [
        
        #ESSENTIAL BINDINGS#

        Key([mod], "Return",
            lazy.spawn(myTerm),
            desc='Launches my Terminal'
            ),
        Key([mod, "shift"], "Return",
            lazy.run_extension(extension.DmenuRun(
                command="dmenu_run",
                dmenu_prompt=">",
                dmenu_font="Ubuntu",
                background="#2D2D2D",
                foreground="#79d4d5",
                selected_background="#79d4d5",
                selected_foreground="#2D2D2D",
                fontsize=5
                ))),
        Key([mod, "shift"], "q",
            lazy.shutdown(),
            desc='Shut Down Qtile'
            ),
        Key([mod], "t",
            lazy.spawn("thunar"),
            desc='Launch Thunar'
            ),
        Key([mod, "shift"], "r",
            lazy.restart(),
            desc='Restart Qtile'
            ),
        Key([mod, "shift"], "c",
            lazy.window.kill(),
            desc='Kill active window'
            ),
        Key([mod], "b",
             lazy.spawn(myBrowser),
             desc='launch my Browser'
             ),
         Key([mod], "Tab",
             lazy.next_layout(),
             desc='Toggle through layouts'
             ),
         Key([mod, "shift"], "c",
             lazy.window.kill(),
             desc='Kill active window'
             ),
         Key([mod, "shift"], "x",
             lazy.spawn("lxsession-logout"),
             desc='Session options'
             ),

         ### Window controls
         Key([mod], "j",
             lazy.layout.down(),
             desc='Move focus down in current stack pane'
             ),
         Key([mod], "k",
             lazy.layout.up(),
             desc='Move focus up in current stack pane'
             ),
         Key([mod], "h",
             lazy.layout.left(),
             desc='Move focus left in current stack pane'
             ),
         Key([mod], "l",
             lazy.layout.right(),
             desc='Move focus right in current stack pane'
             ),
         Key([mod, "shift"], "j",
             lazy.layout.shuffle_down(),
             lazy.layout.section_down(),
             desc='Move windows down in current stack'
             ),
         Key([mod, "shift"], "k",
             lazy.layout.shuffle_up(),
             lazy.layout.section_up(),
             desc='Move windows up in current stack'
             ),
        Key([mod, "control"], "l",
            lazy.layout.grow_right(),
            lazy.layout.grow(),
            lazy.layout.increase_ratio(),
            lazy.layout.delete(),
            ),
        Key([mod, "control"], "h",
            lazy.layout.grow_left(),
            lazy.layout.shrink(),
            lazy.layout.decrease_ratio(),
            lazy.layout.add(),
            ),
        Key([mod, "control"], "k",
            lazy.layout.grow_up(),
            lazy.layout.grow(),
            lazy.layout.decrease_nmaster(),
            ),
        Key([mod, "control"], "j",
            lazy.layout.grow_down(),
            lazy.layout.shrink(),
            lazy.layout.increase_nmaster(),
            ),
         Key([mod], "n",
             lazy.layout.normalize(),
             desc='normalize window size ratios'
             ),
         Key([mod], "m",
             lazy.layout.maximize(),
             desc='toggle window between minimum and maximum sizes'
             ),
         Key([mod, "shift"], "f",
             lazy.window.toggle_floating(),
             desc='toggle floating'
             ),
         Key([mod], "f",
             lazy.window.toggle_fullscreen(),#282C35
             desc='toggle fullscreen'
             ),
         ### Stack controls
         Key([mod, "shift"], "Tab",
             lazy.layout.rotate(),
             lazy.layout.flip(),
             desc='Switch which side main pane occupies (XmonadTall)'
             ),
          Key([mod], "space",
             lazy.layout.next(),
             desc='Switch window focus to other pane(s) of stack'
             ),
         Key([mod, "shift"], "space",
             lazy.layout.toggle_split(),
             desc='Toggle between split and unsplit sides of stack'
             ),
        #Screenshots
        Key([], "Print",
            lazy.spawn("xfce4-screenshooter -f"),
            desc='Take a Screenshot'
            ),
        Key([mod], "Print",
            lazy.spawn("xfce4-screenshooter -r"),
            desc='Printscreen a Region'
            ),
        Key([mod, "shift"], "Print",
            lazy.spawn("xfce4-screenshooter -w"),
            desc='Screenshot active window'
            ),
        
    ]

groups = []

# FOR QWERTY KEYBOARDS
group_names = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0",]

group_labels = ["WEB ", "DEV ", "REC ", "VID ", "MUS ", "MSG ", "DIR ", "GFX ", "TXT ", "GAM ",]
#group_labels = ["", "", "", "", "", "", "", "", "", "",]
#group_labels = ["Web", "Edit/chat", "Image", "Gimp", "Meld", "Video", "Vb", "Files", "Mail", "Music",]

group_layouts = ["monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall",]
#group_layouts = ["monadtall", "matrix", "monadtall", "bsp", "monadtall", "matrix", "monadtall", "bsp", "monadtall", "monadtall",]

for i in range(len(group_names)):
    groups.append(
        Group(
            name=group_names[i],
            layout=group_layouts[i].lower(),
            label=group_labels[i],
        ))

for i in groups:
    keys.extend([

#CHANGE WORKSPACES
        Key([mod], i.name, lazy.group[i.name].toscreen()),
        #Key([mod], "Tab", lazy.screen.next_group()),
        #Key([mod, "shift" ], "Tab", lazy.screen.prev_group()),
        #Key(["mod1"], "Tab", lazy.screen.next_group()),
        #Key(["mod1", "shift"], "Tab", lazy.screen.prev_group()),

# MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND STAY ON WORKSPACE
        #Key([mod, "shift"], i.name, lazy.window.togroup(i.name)),
# MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND FOLLOW MOVED WINDOW TO WORKSPACE
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name) , lazy.group[i.name].toscreen()),
    ])

def init_layout_theme():
    return {"margin":9,
            "border_width": 3,
            "border_focus": "#79D4D5",
            "border_normal": "#2D2D2D"
            }

layout_theme = init_layout_theme()


layouts = [


    layout.MonadTall(**layout_theme),
    layout.MonadWide(**layout_theme),
    layout.Zoomy(
        columnwidth = 150,
        margin = 5,
        property_big = "1.0",
        property_small = "0.1"
        ),
    layout.Bsp(**layout_theme),
    layout.RatioTile(**layout_theme),
    layout.Matrix(**layout_theme),
    layout.Max(**layout_theme),
    layout.Floating(**layout_theme)

]

# COLORS FOR THE BAR
#Theme name : ArcoLinux Default
def init_colors():
    return [["#2D2D2D", "#2D2D2D"], # color 0
            ["#646464", "#646464"], # color 1
            ["#f58e8e", "#f58e8e"], # color 2
            ["#a9d3ab", "#a9d3ab"], # color 3
            ["#fed37e", "#fed37e"], # color 4
            ["#7aabd4", "#7aabd4"], # color 5
            ["#d6add5", "#d6add5"], # color 6
            ["#79d4d5", "#79d4d5"], # color 7
            ["#d4d4d4", "#d4d4d4"], # color 8
            ["#dbaada", "#dbaada"]] # color 9


colors = init_colors()


# WIDGETS FOR THE BAR

widget_defaults = dict(
    font="Ubuntu Mono",
    fontsize = 12,
    padding = 2,
    background=colors[2]
)
extension_defaults = widget_defaults.copy()


def init_widgets_list():
    widgets_list = [
               widget.Sep(
                        linewidth = 0,
                        padding = 6,
                        foregroud = colors[0],
                        background = colors[0]
                        ),
               widget.Image(
                        filename = "~/.config/qtile/icons/arch-light.png",
                        scale = "True",
                        mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e paru')},
                        margin_y = 2,
                        margin_x = 2,
                        padding = 4,
                        background = colors[0]
                        ),
               widget.Sep(
                        linewidth = 0,
                        padding = 6,
                        foreground = colors[2],
                        background = colors [0]
                        ),
               widget.GroupBox(
                       font = "Ubuntu Bold",
                       fontsize = 9,
                       margin_y = 5,
                       margin_x = 0,
                       padding_y = 5,
                       padding_x = 3,
                       borderwidth = 3,
                       active = colors[7],
                       inactive = colors[6],
                       block_highlight_text_color = colors[0],
                       rounded = False,
                       highlight_color = colors[6],
                       highlight_method = "line",
                       this_current_screen_border = colors[0],
                       this_screen_border = colors [7],
                       other_current_screen_border = colors[6],
                       other_screen_border = colors[7],
                       foreground = colors[2],
                       background = colors[0]
                       ),
               widget.Sep(
                        linewidth = 0,
                        padding = 10,
                        foreground = colors[2],
                        background = colors[0]
                        ),
               widget.WindowName(
                        font="Ubuntu Bold",
                        fontsize = 10,
                        foreground = colors[7],
                        background = colors[0],
                        ),
               widget.Sep(
                        linewidth = 0,
                        padding = 6,
                        foreground = colors[2],
                        background = colors [6]
                        ),
               widget.CurrentLayout(
                        font="Ubuntu Regular",
                        fontsize= 11,
                        foreground = colors[0],
                        background = colors [6]
                        ),
               widget.CurrentLayoutIcon(
                       custom_icon_paths = [os.path.expanduser("~/.config/qtile/layout-icons-dark")],
                       foreground = colors[0],
                       background = colors[6],
                       padding = 0,
                       scale = 0.7
                       ),
               widget.Sep(
                        linewidth = 0,
                        padding = 6,
                        foreground = colors[2],
                        background = colors [6]
                        ),
               widget.Sep(
                        linewidth = 0,
                        padding = 6,
                        foreground = colors[2],
                        background = colors [7]
                        ),
               widget.TextBox(
                        text = "MEM:",
                        font = "Ubuntu Regular",
                        mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e htop')},
                        fontsize = 11,
                        foreground = colors[0],
                        background = colors[7]
                        ),
               widget.MemoryGraph(
                        background = colors[7],
                        border_color = colors[7],
                        border_width = 2,
                        mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e htop')},
                        samples = 100,
                        fill_color = colors[6],
                        frequency = 1,
                        graph_color = colors[0],
                        line_width = 3,
                        start_pos = 'bottom'
                        ),
               widget.Sep(
                        linewidth = 0,
                        padding = 6,
                        foreground = colors[2],
                        background = colors [7]
                        ),
               widget.Sep(
                        linewidth = 0,
                        padding = 6,
                        foreground = colors[2],
                        background = colors [6]
                        ),
               widget.TextBox(
                        text = "CPU:",
                        font = "Ubuntu Regualar",
                        mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e htop')},
                        fontsize = 11,
                        foreground = colors[0],
                        background = colors[6]
                        ),
               widget.CPUGraph(
                        background = colors[6],
                        border_color = colors[6],
                        border_width = 2,
                        core = "all",
                        mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e htop')},
                        samples = 100,
                        fill_color = colors[7],
                        frequency = 1,
                        graph_color = colors[0],
                        line_width = 3,
                        start_pos = 'bottom'
                        ),
               widget.Sep(
                        linewidth = 0,
                        padding = 6,
                        foreground = colors[2],
                        background = colors [6]
                        ),
               widget.Sep(
                        linewidth = 0,
                        padding = 6,
                        foreground = colors[2],
                        background = colors [7]
                        ),
               widget.Clock(
                        font="Ubuntu Regular",
                        foreground = colors[0],
                        background = colors[7],
                        fontsize = 11,
                        mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e calcurse')},
                        format="%A, %B %d - %H:%M"
                        ),
               widget.Sep(
                        linewidth = 0,
                        padding = 6,
                        foreground = colors[2],
                        background = colors [7]
                        ),
               widget.Sep(
                        linewidth = 0,
                        padding = 6,
                        foreground = colors[2],
                        background = colors [6]
                        ),
               widget.Systray(
                        background=colors[6],
                        icon_size=20,
                        padding = 4
                        ),
               widget.Sep(
                        linewidth = 0,
                        padding = 6,
                        foreground = colors[2],
                        background = colors [6]
                        ),
              ]
    return widgets_list

widgets_list = init_widgets_list()


def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    return widgets_screen1

def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    return widgets_screen2

widgets_screen1 = init_widgets_screen1()
widgets_screen2 = init_widgets_screen2()


def init_screens():
    return [Screen(top=bar.Bar(widgets=init_widgets_screen1(), size=21, opacity=1)),
            Screen(top=bar.Bar(widgets=init_widgets_screen2(), size=26, opacity=1))]
screens = init_screens()


# MOUSE CONFIGURATION
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []

#HOOKS 



main = None

@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.call([home])

@hook.subscribe.startup
def start_always():
    # Set the cursor to something sane in X
    subprocess.Popen(['xsetroot', '-cursor_name', 'left_ptr'])

@hook.subscribe.client_new
def set_floating(window):
    if (window.window.get_wm_transient_for()
            or window.window.get_wm_type() in floating_types):
        window.floating = True

floating_types = ["openmw", "notification", "toolbar", "splash", "dialog"]


follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules, 
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
    Match(wm_class='Arcolinux-welcome-app.py'),
    Match(wm_class='Arcolinux-tweak-tool.py'),
    Match(wm_class='Arcolinux-calamares-tool.py'),
    Match(wm_class='confirm'),
    Match(wm_class='dialog'),
    Match(wm_class='download'),
    Match(wm_class='error'),
    Match(wm_class='file_progress'),
    Match(wm_class='notification'),
    Match(wm_class='splash'),
    Match(wm_class='toolbar'),
    Match(wm_class='Arandr'),
    Match(wm_class='feh'),
    Match(wm_class='Galculator'),
    Match(wm_class='arcolinux-logout'),
    Match(wm_class='xfce4-terminal'),

],  fullscreen_border_width = 0, border_width = 0)
auto_fullscreen = True

focus_on_window_activation = "focus" # or smart

wmname = "LG3D"
